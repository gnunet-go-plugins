# This file is part of gnunet-go-plugins, a collection of plugins for
# the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
# implement type-specific processing of resource records (e.g. GUI).
# Copyright (C) 2022 by the authors:
#
# * Bernd Fix <brf@hoi-polloi.org>  >Y<
#
# gnunet-go-plugins is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# gnunet-go is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL3.0-or-later

SUBDIRS = example-c example-go reclaimid

all:
	@for i in $(SUBDIRS); do (cd $$i; $(MAKE) $(MFLAGS) $(MYMAKEFLAGS) all); done

clean:
	@for i in $(SUBDIRS); do (cd $$i; $(MAKE) $(MFLAGS) $(MYMAKEFLAGS) clean); done

test:
	@for i in $(SUBDIRS); do (cd $$i; $(MAKE) $(MFLAGS) $(MYMAKEFLAGS) test); done
