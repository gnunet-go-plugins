// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"encoding/base64"
	"fmt"
	"strings"
)

//----------------------------------------------------------------------
// JSON Web Token (JWT) handling
//----------------------------------------------------------------------

func decodeJWT(in string) string {
	parts := strings.Split(in, ".")
	if len(parts) != 3 {
		return "(invalid:parts)"
	}
	var s1, s2, s3 []byte
	var err error
	if s1, err = base64.StdEncoding.DecodeString(parts[0]); err != nil {
		return "(invalid:header)"
	}
	jwt := fmt.Sprintf("<br>&nbsp;&nbsp;Header: %s", string(s1))
	if s2, err = base64.StdEncoding.DecodeString(parts[1]); err != nil {
		return "(invalid:payload)"
	}
	jwt += fmt.Sprintf("<br>&nbsp;&nbsp;Payload: %s<br>", string(s2))

	if s3, err = base64.StdEncoding.DecodeString(parts[2]); err != nil {
		return "(invalid:signature)"
	}
	return jwt + fmt.Sprintf("<br>&nbsp;&nbsp;Signature: %s<br>", split(string(s3), "&nbsp;&nbsp;", 64))
}
