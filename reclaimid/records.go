// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"bytes"
	"fmt"
	"strings"

	"github.com/bfix/gospel/data"
)

// ReclaimRecord interface impemented by custom types.
type ReclaimRecord interface {
	Value(Utility) (string, error)
	ToMap(Utility) (map[string]string, error)
	FromMap(Utility, map[string]string) error
}

// Get record instance for given type and data
func GetInstance(t uint32, buf []byte) (rec ReclaimRecord, err error) {
	switch t {
	case 65544: // RECLAIM_ATTRIBUTE
		rec = new(ReclaimAttribute)
	case 65545: // RECLAIM_TICKET
	case 65550: // RECLAIM_ATTRIBUTE_REF
	case 65552: // RECLAIM_OIDC_CLIENT
	case 65553: // RECLAIM_OIDC_REDIRECT
	case 65554: // RECLAIM_CREDENTIAL
		rec = new(ReclaimCredential)
	case 65555: // RECLAIM_PRESENTATION
	}
	if buf != nil {
		err = data.Unmarshal(rec, buf)
	}
	return
}

//======================================================================
// Reclaim Resource Records
//======================================================================

//----------------------------------------------------------------------
// Reclaim Attribute
//----------------------------------------------------------------------

// ReclaimAttribute record
type ReclaimAttribute struct {
	Type        uint32 `order:"big"`    // type
	Flags       uint32 `order:"big"`    // flags
	ID          []byte `size:"32"`      // Reclaim identifier
	Attestation []byte `size:"32"`      // Attestation
	NameLen     uint32 `order:"big"`    // length of name
	DataLen     uint32 `order:"big"`    // length of data
	Name        []byte `size:"NameLen"` // name
	Data        []byte `size:"DataLen"` // binary data
}

// Value returns a human-readable representation of the record
func (rec *ReclaimAttribute) Value(inv Utility) (string, error) {
	wrt := new(bytes.Buffer)
	wrt.WriteString(fmt.Sprintf("ID=%s,<br>", encodeBase32GNS(rec.ID)))
	cred := encodeBase32GNS(rec.Attestation)
	if len(strings.ReplaceAll(cred, "0", "")) > 0 {
		wrt.WriteString(fmt.Sprintf("Credential=%s,<br>", cred))
	}
	wrt.WriteString(fmt.Sprintf("Type=%s,<br>", reclaimType(rec.Type)))
	wrt.WriteString(fmt.Sprintf("Flags=%s,<br>", reclaimFlags(rec.Flags)))
	name := string(rec.Name)
	val := string(rec.Data)[:rec.DataLen-1]
	wrt.WriteString(fmt.Sprintf("[%s=%s]", name, val))
	return wrt.String(), nil
}

// ToMap returns the parameter set for the record
func (rec *ReclaimAttribute) ToMap(inv Utility) (params map[string]string, err error) {
	params = make(map[string]string)
	params["reclaim_attribute_id"] = encodeBase32GNS(rec.ID)
	params["reclaim_attribute_credential"] = encodeBase32GNS(rec.Attestation)
	params["reclaim_attribute_type"] = fmt.Sprintf("%d", rec.Type)
	params["reclaim_attribute_flags"] = fmt.Sprintf("%d", rec.Flags)
	params["reclaim_attribute_key"] = string(rec.Name)
	params["reclaim_attribute_value"] = string(rec.Data)[:rec.DataLen-1]
	return
}

// FromMap reconstructs the record attributes from parameter list
func (rec *ReclaimAttribute) FromMap(inv Utility, params map[string]string) error {
	return nil
}

//----------------------------------------------------------------------
// Reclaim Credential
//----------------------------------------------------------------------

// ReclaimCredential is a credential resource record data
type ReclaimCredential struct {
	Type    uint32 `order:"big"`    // credendial type
	Flags   uint32 `order:"big"`    // flags
	ID      []byte `size:"32"`      // credential ID
	NameLen uint16 `order:"big"`    // length of name
	Res1    uint16 `order:"big"`    // reserved
	DataLen uint16 `order:"big"`    // length of data
	Res2    uint16 `order:"big"`    // reserved
	Name    []byte `size:"NameLen"` // name
	Data    []byte `size:"DataLen"` // binary data
}

// Value returns a human-readable representation of the record
func (rec *ReclaimCredential) Value(inv Utility) (string, error) {
	wrt := new(bytes.Buffer)
	wrt.WriteString(fmt.Sprintf("ID=%s,<br>", encodeBase32GNS(rec.ID)))
	wrt.WriteString(fmt.Sprintf("Type=%s,<br>", reclaimType(rec.Type)))
	wrt.WriteString(fmt.Sprintf("Flags=%s,<br>", reclaimFlags(rec.Flags)))
	wrt.WriteString(fmt.Sprintf("[%s=%s]", string(rec.Name), decodeJWT(string(rec.Data))))
	return wrt.String(), nil
}

// ToMap returns the parameter set for the record
func (rec *ReclaimCredential) ToMap(inv Utility) (params map[string]string, err error) {
	params = make(map[string]string)
	params["reclaim_credential_id"] = encodeBase32GNS(rec.ID)
	params["reclaim_credential_type"] = fmt.Sprintf("%d", rec.Type)
	params["reclaim_credential_flags"] = fmt.Sprintf("%d", rec.Flags)
	params["reclaim_credential_key"] = string(rec.Name)
	params["reclaim_credential_value"] = string(rec.Data)
	return
}

// FromMap reconstructs the record attributes from parameter list
func (rec *ReclaimCredential) FromMap(inv Utility, params map[string]string) error {
	return nil
}

//----------------------------------------------------------------------
// Reclaim types and flags
//----------------------------------------------------------------------

// Reclaim types and flags
//
//nolint:stylecheck // my style...
const (
	RECLAIM_TYPE_STRING = 1
)

// get the reclaim type name
func reclaimType(t uint32) string {
	switch t {
	case RECLAIM_TYPE_STRING:
		return "STRING"
	}
	return "UNKNOWN"
}

// get reclaim flags as names
func reclaimFlags(f uint32) string {
	if f == 0 {
		return "None"
	}
	s := fmt.Sprintf("%b", f)
	for len(s) < 32 {
		s = "0" + s
	}
	return strings.TrimRight(s, "0")
}
