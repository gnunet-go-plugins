// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	_ "embed"

	"github.com/bfix/gospel/data"
)

//======================================================================
//   C U S T O M I Z A T I O N
//======================================================================

// List of handled resource record types.
var handledTypes = []uint32{
	65544, // RECLAIM_ATTRIBUTE
	65545, // RECLAIM_TICKET
	65550, // RECLAIM_ATTRIBUTE_REF
	65552, // RECLAIM_OIDC_CLIENT
	65553, // RECLAIM_OIDC_REDIRECT
	65554, // RECLAIM_CREDENTIAL
	65555, // RECLAIM_PRESENTATION
}

//----------------------------------------------------------------------
// Interface implementation
//----------------------------------------------------------------------

// Name returns the plugin name (sub-system name)
func (p *CustomPlugin) Name() string {
	return "Re:claimID"
}

// CanHandle returns a list of resource record types that are handled
// by the plugin.
func (p *CustomPlugin) CanHandle() (list []uint32) {
	return handledTypes
}

// Compute a set of record specs allowed under a label with existing records
func (p *CustomPlugin) Compatible(_ string, _ [][2]uint32) [][2]uint32 {
	// simple case: no restrictions (all types, all flags)
	list := make([][2]uint32, 0)
	for _, t := range handledTypes {
		list = append(list, [2]uint32{t, 0})
	}
	return list
}

// TemplateNames returns the names of the new / edit template
// TODO: customize template names (see gui.htpl)
func (p *CustomPlugin) TemplateNames() (string, string) {
	return "new_reclaimid", "edit_reclaimid"
}

// Prefix returns the prefix for record attributes in map
func (p *CustomPlugin) Prefix(t uint32) string {
	switch t {
	case 65544: // RECLAIM_ATTRIBUTE
		return "reclaimid_attr"
	case 65545: // RECLAIM_TICKET
	case 65550: // RECLAIM_ATTRIBUTE_REF
	case 65552: // RECLAIM_OIDC_CLIENT
	case 65553: // RECLAIM_OIDC_REDIRECT
	case 65554: // RECLAIM_CREDENTIAL
	case 65555: // RECLAIM_PRESENTATION
	}
	return "unknown"
}

//======================================================================
// No need to customize code beond this point...
//======================================================================

// Plugin is the instance of a custom implementation accessed by the
// gnunet-go framework
var Plugin = NewCustomPlugin()

// ExamplePlugin is an example plugin for custom records that implements
// the zonemaster.Plugin interface.
type CustomPlugin struct {
	utl Utility
}

// NewCustomPlugin creates an initialized plugin instance
func NewCustomPlugin() CustomPlugin {
	return CustomPlugin{}
}

// SetUtility passes a utility function to the plugin
func (p *CustomPlugin) SetUtility(arg any) {
	var ok bool
	p.utl, ok = arg.(func(fcn string, args ...any) any)
	if !ok {
		p.utl = nil
	}
}

//go:embed gui.htpl
var tpl []byte

// Template returns the new / edit template for resource record types
// handled by the plugin.
func (p *CustomPlugin) Template() string {
	return string(tpl)
}

// Value returns a human-readable description of RR data
func (p *CustomPlugin) Value(t uint32, rr []byte) (string, error) {
	rec, err := GetInstance(t, rr)
	if err != nil {
		return "", err
	}
	return rec.Value(p.utl)
}

// ToMap converts resource record data into GUI template variables
func (p *CustomPlugin) ToMap(t uint32, rr []byte) (map[string]string, error) {
	rec, err := GetInstance(t, rr)
	if err != nil {
		return nil, err
	}
	return rec.ToMap(p.utl)
}

// FromMap converts a GUI template variables into resource record data
func (p *CustomPlugin) FromMap(t uint32, vars map[string]string) ([]byte, error) {
	rec, err := GetInstance(t, nil)
	if err != nil {
		return nil, err
	}
	if err = rec.FromMap(p.utl, vars); err != nil {
		return nil, err
	}
	return data.Marshal(rec)
}
