// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"encoding/hex"
	"testing"
)

func TestPluginCanHandle(t *testing.T) {
	list := Plugin.CanHandle()
	if len(list) == 0 {
		t.Fatal("no type list received from plugin")
	}
	for _, rr := range list {
		t.Logf("Can handle %d\n", rr)
	}
}

func TestPluginCompatible(t *testing.T) {
	in := make([][2]uint32, 0)
	out := Plugin.Compatible("@", in)
	for _, s := range out {
		t.Logf("Compatible: type=%d, flags=%d\n", s[0], s[1])
	}
}

func TestPluginPrefix(t *testing.T) {
	for _, rr := range Plugin.CanHandle() {
		t.Logf("Prefix: '%s' (%d)\n", Plugin.Prefix(rr), rr)
	}
}

var (
	recAttr = []byte{
		0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x9a, 0x97, 0xbc, 0x98, 0x67, 0x57, 0x3d, 0x65,
		0x9e, 0x9d, 0x08, 0x62, 0x7a, 0xd3, 0xe3, 0xb9,
		0xb2, 0xe1, 0xce, 0x7e, 0xb8, 0xed, 0x3b, 0x8d,
		0x46, 0xd8, 0xe3, 0xdb, 0xe8, 0xbb, 0x45, 0x8b,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x05, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00,
		0x65, 0x6d, 0x61, 0x69, 0x6c, 0x62, 0x72, 0x66,
		0x40, 0x68, 0x6f, 0x69, 0x2d, 0x70, 0x6f, 0x6c,
		0x6c, 0x6f, 0x69, 0x2e, 0x6f, 0x72, 0x67, 0x00,
	}
)

func TestPluginValueAttribute(t *testing.T) {
	s, err := Plugin.Value(65544, recAttr)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("attribute: value='%s'\n", s)
}

func TestPluginToMap(t *testing.T) {
	/*
		params, err := Plugin.ToMap(90666, rec1)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("ToMap: rec1 -> %v\n", params)

		if params, err = Plugin.ToMap(90667, rec2); err != nil {
			t.Fatal(err)
		}
		t.Logf("ToMap: rec2 -> %v\n", params)
	*/
}

func TestPluginFromMap(t *testing.T) {
	/*
		params := map[string]string{
			"rectype1_text": "foo",
		}
		rr, err := Plugin.FromMap(90666, params)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("FromMap: rr=%s\n", hex.EncodeToString(rr))
		if !bytes.Equal(rr, rec1) {
			t.Fatal("FromMap for rectype1 failed")
		}
		params = map[string]string{
			"rectype2_var1": "35",
			"rectype2_var2": "66",
		}
		if rr, err = Plugin.FromMap(90667, params); err != nil {
			t.Fatal(err)
		}
		t.Logf("FromMap: rr=%s\n", hex.EncodeToString(rr))
		if !bytes.Equal(rr, rec2) {
			t.Fatal("FromMap for rectype2 failed")
		}
	*/
}

func TestPluginAttributeEncoding(t *testing.T) {
	s := "0000008000001T5BYEPR4CBE0T83PG94YV19T571YSBZT9VWMGNZ56FW3KV5V5BZ000000000000000000000000000000000000000000000000000000000M00000KCNPP2TBCC9S6CG38DXMJTW3FDHP6YT9EDXS6E00"
	buf, err := decodeBase32GNS(s)
	if err != nil {
		t.Error(err)
	}
	t.Log(hex.EncodeToString(buf))
}
