// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

// #cgo CFLAGS: -g -Wall
// #include "plugin.h"
import "C"

import (
	"bytes"
	_ "embed"
	"strings"
	"unsafe"
)

//======================================================================
//   C U S T O M I Z A T I O N
//======================================================================

//----------------------------------------------------------------------
// Interface implementation
//----------------------------------------------------------------------

// Name returns the plugin name (sub-system name)
//
// TODO: customize name of the plugin
func (p *CustomPlugin) Name() string {
	return "Example-C"
}

// TemplateNames returns the names of the new / edit template
// TODO: customize template names (see gui.htpl)
func (p *CustomPlugin) TemplateNames() (string, string) {
	return "new_myrecords", "edit_myrecords"
}

//======================================================================
// No need to customize code beond this point...
//======================================================================

// Plugin is the instance of a custom implementation accessed by the
// gnunet-go framework
var Plugin = NewCustomPlugin()

// ExamplePlugin is an example plugin for custom records that implements
// the zonemaster.Plugin interface.
type CustomPlugin struct {
	num int // number of managed resource record types
}

// NewCustomPlugin creates an initialized plugin instance
func NewCustomPlugin() CustomPlugin {
	return CustomPlugin{}
}

// SetUtility passes a utility function to the plugin
// Not used in C-implementation
func (p *CustomPlugin) SetUtility(arg any) {
}

//go:embed gui.htpl
var tpl []byte

// Template returns the new / edit template for resource record types
// handled by the plugin.
func (p *CustomPlugin) Template() string {
	return string(tpl)
}

// CanHandle returns a list of resource record types that are handled
// by the plugin. Calls the a C function from 'plugin.c'
func (p *CustomPlugin) CanHandle() (list []uint32) {
	var out *C.uint32_t
	num := C.can_handle(out)
	p.num = int(num)
	out = (*C.uint32_t)(C.malloc(C.ulong(p.num * 4)))
	defer C.free(unsafe.Pointer(out))
	C.can_handle(out)
	clist := unsafe.Slice(out, p.num)
	for _, v := range clist {
		list = append(list, uint32(v))
	}
	return
}

// Compute a set of record specs allowed under a label with existing records.
// Calls a C-function from 'plugin.c'
func (p *CustomPlugin) Compatible(label string, rrSpecs [][2]uint32) (rrOut [][2]uint32) {
	// the result array can't be larger than the number of types managed
	out := (*C.uint32_t)(C.malloc(C.ulong(p.num * 8)))
	defer C.free(unsafe.Pointer(out))
	// get label
	labelC := C.CString(label)
	defer C.free(unsafe.Pointer(labelC))
	// get incoming RR specs
	inNum := len(rrSpecs)
	in := (*C.uint32_t)(C.malloc(C.ulong(inNum * 8)))
	defer C.free(unsafe.Pointer(in))

	// call plugin and assemble result
	outNum := C.compatible(labelC, in, out)
	clist := unsafe.Slice(out, 2*outNum)
	for i := 0; i < p.num; i++ {
		rrt := uint32(clist[2*i])
		rrf := uint32(clist[2*i+1])
		rrOut = append(rrOut, [2]uint32{rrt, rrf})
	}
	return
}

// Prefix returns the prefix for record attributes in map
// Calls a C-function from 'plugin.c'
func (p *CustomPlugin) Prefix(t uint32) string {
	cs := C.prefix(C.uint32_t(t))
	return C.GoString(cs)
}

// Value returns a human-readable description of RR data
// Calls a C-function from 'plugin.c'
func (p *CustomPlugin) Value(t uint32, rr []byte) (string, error) {
	cs := C.value(C.uint32_t(t), (*C.char)(unsafe.Pointer(&rr[0])))
	return C.GoString(cs), nil
}

// ToMap converts resource record data into GUI template variables
// Calls a C-function from 'plugin.c'
func (p *CustomPlugin) ToMap(t uint32, rr []byte) (params map[string]string, err error) {
	var res *C.char
	num := C.to_map(C.uint32_t(t), (*C.char)(unsafe.Pointer(&rr[0])), &res)
	defer C.free(unsafe.Pointer(res))
	b := make([]byte, num)
	slice := unsafe.Slice(res, num)
	for i := range b {
		b[i] = byte(slice[i])
	}
	list := strings.Split(string(b), "\000")
	params = make(map[string]string)
	for i := 0; i < len(list)/2; i++ {
		params[list[2*i]] = list[2*i+1]
	}
	return
}

// FromMap converts a GUI template variables into resource record data
// Calls a C-function from 'plugin.c'
func (p *CustomPlugin) FromMap(t uint32, vars map[string]string) ([]byte, error) {
	buf := new(bytes.Buffer)
	for k, v := range vars {
		buf.WriteString(k)
		buf.WriteByte(0)
		buf.WriteString(v)
		buf.WriteByte(0)
	}
	buf.WriteByte(0)
	params := buf.Bytes()
	var out *C.char
	num := C.from_map(C.uint32_t(t), (*C.char)(unsafe.Pointer(&params[0])), &out)
	b := make([]byte, num)
	slice := unsafe.Slice(out, num)
	for i := range b {
		b[i] = byte(slice[i])
	}
	return b, nil
}
