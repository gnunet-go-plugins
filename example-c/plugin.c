// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

#include "plugin.h"

#define RECTYPE1 90666
#define RECTYPE2 90667

const char* rectype1 = "rectype1";
const char* rectype2 = "rectype2";

typedef struct {
    uint16_t text_len;
    // followed by string (NOT \0 terminated!)
} REC1;

typedef struct {
    uint32_t var1;
    uint16_t var2;
} REC2;


int can_handle(uint32_t* out) {
    if (out != NULL) {
        // TODO: customize record type list
        out[0] = RECTYPE1;
        out[1] = RECTYPE2;
    }
    return 2;
}

int compatible(const char* label, uint32_t* in, uint32_t* out) {
    // simple case: no restrictions (all handled types allowed, no enforced flags)
    out[0] = RECTYPE1;  // type #1
    out[1] = 0;         // flags #1
    out[2] = RECTYPE2;  // type #2
    out[3] = 0;         // flags #2
    return 2;
}

const char* prefix(uint32_t rr) {
    // TODO: customize record type list
    if (rr == RECTYPE1)
        return rectype1;
    if (rr == RECTYPE2)
        return rectype2;
    return NULL;
}

char* value(uint32_t rrType, char* rrData) {
    char* out = (char*)malloc(1024);
    // TODO: customize record type list
    if (rrType == RECTYPE1) {
        REC1* rec = (REC1*)rrData;
        if (rec->text_len == 0) {
            out[0] = 0;
        } else {
            strcpy(out, rrData+2);
        }
    } else if (rrType == RECTYPE2) {
        REC2* rec = (REC2*)rrData;
        sprintf(out, "var1=%d,<br>var2=%d", ntohl(rec->var1), ntohs(rec->var2));
    }
    return out;
}

int to_map(uint32_t rrType, char* rrData, char** res) {
    *res = (char*)malloc(1024);
    char* out = *res;
    // TODO: customize record type list
    if (rrType == RECTYPE1) {
        REC1* rec = (REC1*)rrData;
        const char* key = "rectype1_text";
        strcpy(out, key);
        out += strlen(key)+1;
        if (rec->text_len == 0) {
            *out = 0;
            out++;
        } else {
            const char* value = rrData+2;
            strncpy(out, value, rec->text_len);
            out += strlen(value);
        }
    } else if (rrType == RECTYPE2) {
        REC2* rec = (REC2*)rrData;
        char* key = "rectype2_var1";
        strcpy(out, key);
        out += strlen(key)+1;
        sprintf(out, "%d", ntohl(rec->var1));
        out += strlen(out)+1;
        key = "rectype2_var2";
        strcpy(out, key);
        out += strlen(key)+1;
        sprintf(out, "%d", ntohs(rec->var2));
        out += strlen(out)+1;
    }
    return out-*res;
}

int from_map(uint32_t rrType, char* params, char** rrData) {
    *rrData = (char*)malloc(1024);
    char* res = *rrData;
    // TODO: customize record type list
    if (rrType == RECTYPE1) {
        printf("*** key='%s'\n", params);
        if (strcmp(params, "rectype1_text") == 0) {
            int n = strlen(params);
            char* value = params+n+1;
            n = strlen(value);
            *(uint16_t*)res = htons(n);
            strcpy(res+2, value);
            return n+3;
        }
    } else if (rrType == RECTYPE2) {
        char* ptr = params;
        REC2* rec = (REC2*)(res);
        int val;
        for (int i = 0; ; i++) {
            int n = strlen(ptr);
            if (n == 0)
                break;
            char* value = ptr+n+1;
            n = strlen(value);
            if (strcmp(ptr, "rectype2_var1") == 0) {
                sscanf(value, "%d", &val);
                rec->var1 = htonl(val);
            } else if (strcmp(ptr, "rectype2_var2") == 0) {
                sscanf(value, "%d", &val);
                rec->var2 = htons((uint16_t)(val));
            }
            ptr = value+n+1;
        }
        return 6;
    }
    return 0;
}
