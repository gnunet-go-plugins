// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

#ifndef _PLUGIN_H
#define _PLUGIN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>

int can_handle(uint32_t*);

int compatible(const char* label, uint32_t* in, uint32_t* out);

const char* prefix(uint32_t rr);

char* value(uint32_t rrType, char* rrData);

int to_map(uint32_t rrType, char* rrData, char** params);

int from_map(uint32_t rrType, char* params, char** rrData);

#endif
