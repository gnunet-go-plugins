# gnunet-go-plugins: Plugins for the GNUnet implementation in Go

Copyright (C) 2022 Bernd Fix  >Y<

gnunet-go-plugins is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

gnunet-go-plugins is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

SPDX-License-Identifier: AGPL3.0-or-later

## Caveat

THIS IS WORK-IN-PROGRESS AT A VERY EARLY STATE. DON'T EXPECT ANY COMPLETE
DOCUMENTATION OR COMPILABLE, RUNNABLE OR EVEN OPERATIONAL SOURCE CODE.

## Introduction

The ZoneMaster implementation of GNUnet-Go provides a GUI to manage (display,
create, edit and delete) GNS zones (identities), zone label and resource
records below a label. The framework implementation only support GNS (and
some DNS) record types, so custom record types (e.g. for Re:claimID or
filesharing) cannot be displayed, created or edited (only deleted!).

This gap can be closed by custom plugins that handle custom resource record
types. The minimal implementation provides a function to convert the record
data into human-readable format (string) suitable to be displayed on a HTML
page. Advanced versions also allow creation of new records and editing
existing records too.

The main plugin code must be written in Go1.18+ and compiled with the exact
same Go version as the framework using it (zonemaster-go). Differences in
versions will result in unloadable plugins.

Two examples are provided: `example-c` and `example-go`. You start with
copying the example to a new folder for your custom plugin and customizing
the code to your needs. The examples are rudimentary and don't cover real-world
resource records for more complex sub-systems.

# Anatomy of a ZoneMaster plugin

A plugin consists of two parts: A set of methods to handle the custom
resource records and a HTML template for the records that integrates into
the ZoneMaster GUI framework.

It is important that every name you use in your custom plugin is unique;
_never_ share names across plugins or resource record attributes!

# Plugin methods

* `Name() string`: returns the name of the plugin. It is recommended that
  the plugin name is using the common name of the GNUnet sub-system the
  resource records belong to (e.g. "Re:claimID" or "filesharing")

* `CanHandle() []uint32`: Returns a list of resource record types the
  plugin can handle. Resource record types _must_ be registered with GANA
  to avoid collisions with other custom plugins.

*	`Compatible(label string, rrSpecs [][2]uint32) ([][2]uint32, error)`:
  Return a list of allowed record types and enforced record flags when
  adding a new resource record under the given label with existing records
  `rrSpecs`.

* `Template() string`: Return the HTML templates used for creating and
  editing custom resource records.

* `TemplateNames() (string,string)`: Returns the names of the "new" and
  "edit" templates in the customized template.

* `Value(rrType uint32, rrData []byte) (string,error)`: Returns the
  human-readable representation of custom resource record data for a given
  type. This is the method that allows the display of the custom resource
  records in the GUI. This method is only called for types registered
  with `CanHandle()`.

* `Prefix(rrType uint32) string`: Returns the prefix used for record attribues
  used in `ToMap()` and `FromMap()` methods as well as in HTML templates.

* `ToMap(rrType uint32, rrData []byte) (map[string]string, error)`: Returns
  a map that includes all record attributes with a string name and a string
  value. The names used for attributes must match the names used in the HTML
  template; the details are explaind in section **HTML templates**. This method
  is only called for types registered with `CanHandle()`.

* `FromMap(rrType uint32, params map[string]string) (rrData []byte, err error)`:
  Returns the binary resource record data for the given type and parameter set.
  The method reconstructs the record attributes from the map and returns the
  binary representation of the resource record data. It uses the same names for
  attributes as the `ToMap()` method. This method is only called for types
  registered with `CanHandle()`.

# Custom resource record types

The ZoneMaster framework can only handle resource record types that are
registered with GANA and are included in the `gnunet-go` framework.

For the examples lets assume that GANA includes:

```
Number: 90666
Name: RECTYPE1
Comment: Custom record type #1

Number: 90667
Name: RECTYPE2
Comment: Custom record type #2
```

The names of the resource record definition are used by the framework
and **must** be used in the plugin (especialy the HTML template) too.

# HTML templates

The probably most challening part of writing a custom plugin is the HTML template
system used for serving the ZoneMaster GUI. It deems necessary to read the first
part (until the "Index" section) of the
[Go text template package documentation](https://pkg.go.dev/text/template); it
describes how the template system works and how you can use it within your own
templates.

In the examples a separate file called `gui.htpl` contains the templates
handled by the custom plugin. It is embedded into the plugin so that no
additional files have to be deployed along with the plugin.

Each GUI page is rendered from HTML templates; the custom plugin must provide
at least two templates: one for creating a new record and one for editing an
existing record. Since most sub-systems rquire multiple record types, the
template for "new record" must have a selection of the desired record type
first before showing the specific GUI elements for a record and the template
for "edit record" must automatically select the matching record template based
on tyoe. The ZoneMaster GUI framework provides helpers to easily do that; this
is explained in sections **"New record" dialog** and **"Edit record dialog**.

For each record type you also need a template to provide HTML form fields for
the record data. See **Resource record dialogs** for more details.

Each template is called with a `Data` object that contains information to
render the final HTML page. The `Data` object has the following fields:

* **Ref int64**: database id of the reference object. In a "new" dialog,
  the ID refers to the parent object (GNS label); in an "edit" dialog it
  refers to the resource record being editied. The ID is used to generate
  custom GET and POST requests.

* **Action string**: "new" or "upd" action. It is used to decide if a
  resource record dialog is in "new" or "edit" mode.

* **Button string**: Recommended prefix for the submit button on record
  dialogs.

* **Names []string**: List of existing names (Zones or Labels) when creating
  or editing zones and labels. It is empty for resource record dialogs.
  
* **RRspecs []*enums.GNSSpec**: list of allowed record types and flags (REC)
  when creating new records as computed by the plugin based on label name
  and existing records (see `Compatible()` plugin method).

* **Params map[string]string**: map of template parameters. Some parameters
  are set by the framework and some parameters correspond to the custom
  resource record.

## Template parameters

The framework automatically sets some template parameters:

If the "New" dialog is called, the following map keys are defined:

* **label**: Name of the label the record is added to. Can be used by
  custom dialogs to decide what record types are allowed for a label
  (e.g. the apex label "@" might enforce special record types).

* **lid**: The database ID of the label (used by the framework)

If the "Edit" dialog is called, the following map keys are defined:

* **label**: Name of the label the record is added to. Can be used by
  custom dialogs to decide what record types are allowed for a label
  (e.g. the apex label "@" might enforce special record types).

* **lid**: The database ID of the label (used by the framework)

* **prefix**: Prefix used for naming custom form fields.

* **type**: Name of the resource record type as defined in GANA

* **created**: The creation timestamp of the record

* **modified**: Timestamp of last modification

* **<prefix>_expires**: Expiration timestamp

* **<prefix>_{private,shadow,suppl}**: Resource record flags (private, shadow,
  supplementary).

`prefix` is defined by the `Prefix()` method of the plugin

## "New record" dialog

You can re-use and customize the example template; it provides all the
boilerplate you don't want to reproduce on your own (although of course you
can). For each custom type you need a template (named after the GANA name
of the record) to handle editing record data.

The HTML page has a tabbed selection of record types you can create; in our
example the tabs will be named "RECTYPE1" and "RECTYPE2". Selecting a tab
switches the rest of the dialog to the selected record type.

```html
{{define "new_myrecords"}}
  <!-- customize template name -->
  {{$data := .}}
  <div>
    <!-- customize heading -->
    <h3>Creating a new record for label "{{index .Params "label"}}":</h3>
    <div class="tabset">
      {{range $i, $type := .RRspecs}}
      <input type="radio" name="tabset" id="tab{{$i}}"
        aria-controls="tab{{$i}}" {{if eq $i 0}}checked{{end}}
      >
      <label for="tab{{$i}}">{{rrtype $type.Type}}</label>
      {{end}}
      <div class="tab-panels">
        {{range $i, $spec := .RRspecs}}
        <section id="tab{{$i}}" class="tab-panel">
          {{$t := rrtype $spec.Type}}
          {{$pf := setspecs $data.Params $spec}}
          <!-- customize here based on GANA values -->
          {{if eq $t "RECTYPE1"}}{{template "RECTYPE1" $data}}{{end}}
          {{if eq $t "RECTYPE2"}}{{template "RECTYPE2" $data}}{{end}}
          <!-- end customize -->
        </section>
        {{end}}
      </div>
    </div>
    <a href="/"><button>Leave</button></a>
  </div>
{{end}}
```

## "Edit record" dialog

Like the new dialog, the edit dialog from the examples define all the
boilerplate to handle edit requests; it can be customized the same way as the
"new" dialog:

```html
{{define "edit_myrecords"}}
  <!-- customize template name -->
  <div>
    <!-- customize heading -->
    <h3>Edit a resource record for label {{index .Params "label"}}:</h3>
    <p>
      <small>
        (Created: {{index .Params "created"}},
        Last edited: {{index .Params "modified"}})
      </small>
    </p>
    {{$t := rritype (index .Params "type")}}
    <!-- customize here based on GANA values -->
    {{if eq $t "RECTYPE1"}}{{template "RECTYPE1" .}}{{end}}
    {{if eq $t "RECTYPE2"}}{{template "RECTYPE2" .}}{{end}}
    <!-- end customize -->
  </div>
  <a href="/"><button>Leave</button></a>
{{end}}
```

## Resource record dialogs

For each custom record type you need to create a shared template to be used
for creating and editing a specific custom record type.

```html
{{define "RECTYPE1"}}
  <h3>RECTYPE1</h3>
  <form action="/action/{{.Action}}/rr/{{.Ref}}"
    {{if eq .Action "upd"}}method="post"{{end}}
  >
    <input type="hidden" name="type" value="{{index .Params "type"}}">
    <table>
      <!-- customization begins -->
      <tr>
        <td align="right"><b>Attribute1:</b></td>
        <td>
          <input type="text" name="rectype1_text"
              maxlength="63" size="63"
              autofocus required
              value="{{index .Params "rectype1_text"}}"
          >
        </td>
      </tr>
      <!-- customization ends -->
      {{template "RRCommon" .}}
      <tr><td/><td><button id="submit">{{.Button}} record</button></td></tr>
    </table>
  </form>
  <script>
    // add JavaScript here if required
  </script>
{{end}}
```

Customizing a record dialog adds fields for each of the attributes of
the record; this can be plain text, checkboxes, ranges or an other fancy
HTML form element. If required you can even add JavaScript to provide
interactive behaviour of form fields (or to validate values beyond HTML
form validation).

The framework automatically handles common parameters like expiration
time or flags; you don't need to add that to our custom record templates.

The most important issue about resource record dialogs is that the names
of the form fields must be _unique across all record types_ handled by the
plugin. This happens "automatically" by using a type-specific prefix (that
is returnd by the plugin with the `Prefix()` method).

# Mapping resource record attributes

The main task of a plugin is to create a parameter map from its attributes
and to re-create attributes from a parameter map.

## `ToMap()` method

Let's assume we have a simple record with type RECTYPE1 that only contains
a string attribute:

```go
type MyRecord1 struct {
	TxtLen uint16 `order:"big"`   // length of text in NBO
	Text   []byte `size:"TxtLen"` // text data (not terminated by \0)
}
```

If the `Prefix()`method returns "myrec1" for this type, the name of the
exposed text attribute might be "myrec1_text" (this is what is used in the
example HTML template). The `ToMap()` method (for the type) is simply:

```go
func (rec *MyRecord1) ToMap() (params map[string]string, err error) {
	params = make(map[string]string)
	params["myrec1_text"] = string(rec.Text)
	return
}
```

## `FromMap()` method

The `FromMap()` method is the reverse of `ToMap()`; it reconstructs
record attributes from a parameter map:

```go
func (rec *MyRecord1) FromMap(params map[string]string) error {
	text, ok := params["myrec1_text"]
	if !ok {
		return errors.New("myrec1_text missing")
	}
	rec.TxtLen = uint16(len(text))
	rec.Text = []byte(text)
	return nil
}
```
