// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"errors"
	"fmt"
	"strconv"
)

//----------------------------------------------------------------------
// MyRecord1 (example custom record #1)
//----------------------------------------------------------------------

// MyRecord1 is an example record, where the record data is a simple
// text string.
type MyRecord1 struct {
	TxtLen uint16 `order:"big"`   // length of text in NBO
	Text   []byte `size:"TxtLen"` // text data (not terminated by \0)
}

// Value returns a human-readable representation of the record
func (rec *MyRecord1) Value(_ Utility) (string, error) {
	return string(rec.Text), nil
}

// ToMap returns the parameter set for the record
func (rec *MyRecord1) ToMap(_ Utility) (params map[string]string, err error) {
	params = make(map[string]string)
	params["rectype1_text"] = string(rec.Text)
	return
}

// FromMap reconstructs the record attributes from parameter list
func (rec *MyRecord1) FromMap(_ Utility, params map[string]string) error {
	text, ok := params["rectype1_text"]
	if !ok {
		return errors.New("rectype1_text missing")
	}
	rec.TxtLen = uint16(len(text))
	rec.Text = []byte(text)
	return nil
}

//----------------------------------------------------------------------
// MyRecord2 (example custom record #2)
//----------------------------------------------------------------------

// MyRecord2 is an example record, where the record data contains two
// integer values
type MyRecord2 struct {
	Var1 uint32 `order:"big"` // variable #1 in NBO
	Var2 uint16 `order:"big"` // variable #2 in NBO
}

// Value returns a human-readable representation of the record
func (rec *MyRecord2) Value(_ Utility) (string, error) {
	return fmt.Sprintf("var1=%d,<br>var2=%d", rec.Var1, rec.Var2), nil
}

// ToMap returns the parameter set for the record
func (rec *MyRecord2) ToMap(_ Utility) (params map[string]string, err error) {
	params = make(map[string]string)
	params["rectype2_var1"] = strconv.Itoa(int(rec.Var1))
	params["rectype2_var2"] = strconv.Itoa(int(rec.Var2))
	return
}

// FromMap reconstructs the record attributes from parameter list
func (rec *MyRecord2) FromMap(_ Utility, params map[string]string) (err error) {
	if rec.Var1, err = asInt[uint32](params, "rectype2_var1"); err != nil {
		return
	}
	rec.Var2, err = asInt[uint16](params, "rectype2_var2")
	return
}
