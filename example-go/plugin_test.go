// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"bytes"
	"encoding/hex"
	"testing"
)

func TestPluginCanHandle(t *testing.T) {
	list := Plugin.CanHandle()
	if len(list) == 0 {
		t.Fatal("no type list received from plugin")
	}
	for _, rr := range list {
		t.Logf("Can handle %d\n", rr)
	}
}

func TestPluginCompatible(t *testing.T) {
	in := make([][2]uint32, 0)
	out := Plugin.Compatible("@", in)
	for _, s := range out {
		t.Logf("Compatible: type=%d, flags=%d\n", s[0], s[1])
	}
}

func TestPluginPrefix(t *testing.T) {
	t.Logf("Prefix: '%s' (%d)\n", Plugin.Prefix(90666), 90666)
	t.Logf("Prefix: '%s' (%d)\n", Plugin.Prefix(90667), 90667)
}

var (
	rec1 = []byte{0x00, 0x03, 0x66, 0x6f, 0x6f}
	rec2 = []byte{0x00, 0x00, 0x00, 0x23, 0x00, 0x42}
)

func TestPluginValue(t *testing.T) {
	s1, err := Plugin.Value(90666, rec1)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("rec1: text='%s'\n", s1)

	s2, err := Plugin.Value(90667, rec2)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("rec2: '%s'\n", s2)
}

func TestPluginToMap(t *testing.T) {
	params, err := Plugin.ToMap(90666, rec1)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("ToMap: rec1 -> %v\n", params)

	if params, err = Plugin.ToMap(90667, rec2); err != nil {
		t.Fatal(err)
	}
	t.Logf("ToMap: rec2 -> %v\n", params)
}

func TestPluginFromMap(t *testing.T) {
	params := map[string]string{
		"rectype1_text": "foo",
	}
	rr, err := Plugin.FromMap(90666, params)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("FromMap: rr=%s\n", hex.EncodeToString(rr))
	if !bytes.Equal(rr, rec1) {
		t.Fatal("FromMap for rectype1 failed")
	}
	params = map[string]string{
		"rectype2_var1": "35",
		"rectype2_var2": "66",
	}
	if rr, err = Plugin.FromMap(90667, params); err != nil {
		t.Fatal(err)
	}
	t.Logf("FromMap: rr=%s\n", hex.EncodeToString(rr))
	if !bytes.Equal(rr, rec2) {
		t.Fatal("FromMap for rectype2 failed")
	}
}
