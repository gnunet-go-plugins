// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go-plugons is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	"encoding/base32"
	"fmt"
	"strconv"
	"strings"
)

// Utility function for exported ZoneMaster utility helpers
type Utility func(fcn string, args ...any) any

// GNSType returns the name of a GNSType
func (u Utility) GNSType(t uint32) string {
	rtype, ok := u("gns_type_name", t).(string)
	if !ok {
		return fmt.Sprintf("GNSType(%d)", t)
	}
	return rtype
}

// GNSFlags returns the list of GNS flags by name
func (u Utility) GNSFlags(f uint32) string {
	flags, ok := u("gns_flags", f).([]string)
	if !ok {
		return fmt.Sprintf("GNSFlags(%d)", f)
	}
	if len(flags) == 0 {
		return "None"
	}
	return strings.Join(flags, ",")
}

// Base32GNS encoding character set
var cf32Enc = base32.NewEncoding("0123456789ABCDEFGHJKMNPQRSTVWXYZ")

// Return a byte array encoded with Base32GNS
//
//nolint:deadcode // might be used, might be not...
func encodeBase32GNS(buf []byte) string {
	return strings.TrimRight(cf32Enc.EncodeToString(buf), "=")
}

// Convert string to byte array using Base32GNS
//
//nolint:deadcode // might be used, might be not...
func decodeBase32GNS(s string) ([]byte, error) {
	s = strings.ToUpper(s)
	s = strings.NewReplacer("O", "0", "I", "1", "L", "1", "U", "V").Replace(s)
	return cf32Enc.DecodeString(s)
}

// Numbers convertable from parameter value (string)
type Number interface {
	uint16 | uint32 | int | int16 | int32 | float32 | float64
}

// convert parameter value (string) to given number type
func asInt[T Number](params map[string]string, key string) (val T, err error) {
	vs, ok := params[key]
	if !ok {
		err = fmt.Errorf("%s missing", key)
		return
	}
	var v int
	if v, err = strconv.Atoi(vs); err != nil {
		return
	}
	return T(v), nil
}

// Split a string into lines of given length
//
//nolint:deadcode // might be unused
func split(s, prefix string, chunk int) (out string) {
	for len(s) > chunk {
		if len(out) > 0 {
			out += ",<br>"
		}
		out += prefix + s[:chunk]
		s = s[chunk:]
	}
	if len(out) > 0 {
		out += ",<br>"
	}
	out += prefix + s
	return
}
