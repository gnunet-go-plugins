// This file is part of gnunet-go-plugins, a collection of plugins for
// the GNUnet-implementation in Golang (gnunet.go). Plugins are used to
// implement type-specific processing of resource records (e.g. GUI).
// Copyright (C) 2022 by the authors:
//
// * Bernd Fix <brf@hoi-polloi.org>  >Y<
//
// gnunet-go-plugins is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// gnunet-go is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main

import (
	_ "embed"

	"github.com/bfix/gospel/data"
)

//======================================================================
//   C U S T O M I Z A T I O N
//======================================================================

// list of custom record types
//
// TODO: list record types handled by the plugin
const (
	RECTYPE1 = 90666
	RECTYPE2 = 90667
)

//----------------------------------------------------------------------
// Interface implementation
//----------------------------------------------------------------------

// Name returns the plugin name (sub-system name)
//
// TODO: customize name of the plugin
func (p *CustomPlugin) Name() string {
	return "Example-Go"
}

// CanHandle returns a list of resource record types that are handled
// by the plugin.
//
// TODO: customize record type list
func (p *CustomPlugin) CanHandle() (list []uint32) {
	return []uint32{
		RECTYPE1,
		RECTYPE2,
	}
}

// Compute a set of record specs allowed under a label with existing records
func (p *CustomPlugin) Compatible(label string, rrSpecs [][2]uint32) [][2]uint32 {
	// simple case: no restrictions (all types, all flags)
	list := [][2]uint32{
		{RECTYPE1, 0},
		{RECTYPE2, 0},
	}
	return list
}

// TemplateNames returns the names of the new / edit template
// TODO: customize template names (see gui.htpl)
func (p *CustomPlugin) TemplateNames() (string, string) {
	return "new_myrecords", "edit_myrecords"
}

// Prefix returns the prefix for record attributes in map
func (p *CustomPlugin) Prefix(t uint32) string {
	switch t {
	case RECTYPE1:
		return "rectype1"
	case RECTYPE2:
		return "rectype2"
	}
	return ""
}

//======================================================================
// No need to customize code beond this point...
//======================================================================

// Plugin is the instance of a custom implementation accessed by the
// gnunet-go framework
var Plugin = NewCustomPlugin()

// ExamplePlugin is an example plugin for custom records that implements
// the zonemaster.Plugin interface.
type CustomPlugin struct {
	utl Utility
}

// NewCustomPlugin creates an initialized plugin instance
func NewCustomPlugin() CustomPlugin {
	return CustomPlugin{}
}

// SetUtility passes a utility function to the plugin
func (p *CustomPlugin) SetUtility(arg any) {
	var ok bool
	p.utl, ok = arg.(func(fcn string, args ...any) any)
	if !ok {
		p.utl = nil
	}
}

//go:embed gui.htpl
var tpl []byte

// Template returns the new / edit template for resource record types
// handled by the plugin.
func (p *CustomPlugin) Template() string {
	return string(tpl)
}

// Value returns a human-readable description of RR data
func (p *CustomPlugin) Value(t uint32, rr []byte) (string, error) {
	rec, err := p.GetInstance(t, rr)
	if err != nil {
		return "", err
	}
	return rec.Value(p.utl)
}

// ToMap converts resource record data into GUI template variables
func (p *CustomPlugin) ToMap(t uint32, rr []byte) (map[string]string, error) {
	rec, err := p.GetInstance(t, rr)
	if err != nil {
		return nil, err
	}
	return rec.ToMap(p.utl)
}

// FromMap converts a GUI template variables into resource record data
func (p *CustomPlugin) FromMap(t uint32, vars map[string]string) ([]byte, error) {
	rec, err := p.GetInstance(t, nil)
	if err != nil {
		return nil, err
	}
	if err = rec.FromMap(p.utl, vars); err != nil {
		return nil, err
	}
	return data.Marshal(rec)
}

// CustomRecord interface impemented by custom types.
type CustomRecord interface {
	Value(Utility) (string, error)
	ToMap(Utility) (map[string]string, error)
	FromMap(Utility, map[string]string) error
}

// Get record instance for given type and data
// TODO: customize to own needs
func (p *CustomPlugin) GetInstance(t uint32, buf []byte) (rec CustomRecord, err error) {
	switch t {
	case RECTYPE1:
		rec = new(MyRecord1)
	case RECTYPE2:
		rec = new(MyRecord2)
	}
	if buf != nil {
		err = data.Unmarshal(rec, buf)
	}
	return
}
